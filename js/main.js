'use strict';
import Statistic from './components/Statistic.js';
import UserList from './components/UserList.js';

class App {
    constructor() {
    	this.body = document.querySelector('body');
        this.app = document.getElementById('app');
        this.button = this.app.querySelector('[data-app-button]');
        this.apiUrl = 'https://randomuser.me/api/';

        this.init();
    }

    init() {
    	this.button.onclick = () => {
    		this.preloader();
			this.getUserList();
		}
	}

	render(users) {
		new UserList(users);
		new Statistic(users);
	}

	getUserList() {
		let scope = this;
		if(typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1){
			fetch(scope.apiUrl + '?results=' + scope.countUsers)
				.then(response => response.json())
				.then(json => {
					scope.render(json);
					window.setTimeout(() => {
						this.preloader(false);
					}, 500);
				})
				.catch(e => {
					new Error(e.message);
					window.setTimeout(() => {
						this.preloader(false);
					}, 500);
				})
		} else {
			let xhr = new XMLHttpRequest();
			xhr.open('GET', scope.apiUrl + '?results=' + scope.countUsers);
			xhr.onload = () => {
				if (xhr.readyState === 4) {
					try {
						scope.render(xhr.responseText);
						window.setTimeout(() => {
							this.preloader(false);
						}, 500);
					} catch (e) {
						new Error(e.message);
						window.setTimeout(() => {
							this.preloader(false);
						}, 500);
					}
				}
			};
			xhr.onerror = (e) => {
				new Error(e.message);
				window.setTimeout(() => {
					this.preloader(false);
				}, 500);
			};
			xhr.send();
		}
	}

	preloader(show = true) {
    	if (show) {
			this.body.classList.add('loader');
			return false;
		}

		this.body.classList.remove('loader');
	}

    get countUsers() {
        let min = 0;
        let max = 100;
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}


window.addEventListener('load', function () {
    new App();
});

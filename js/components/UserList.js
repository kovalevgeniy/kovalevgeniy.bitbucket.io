'use strict';

class UserList {
	constructor(props) {
		this.usersList = props;
		this.app = document.getElementById('app');
		this.table = null;
		this.tableBody = null;
		this.init();
	}

	init() {
		this.app.innerHTML = window.tmpl('userList', {});
		this.table = this.app.querySelector('[data-table]');
		this.tableBody = this.table.querySelector('[data-table-body]');
		this.render();
	}

	render() {
		let html = '';

		for (let key in this.usersList.results) {
			let user = this.usersList.results[key];
			user.phone = this.maskPhone(user.phone);
			user.dob.date = this.checkDate(user.dob.date);
			user.registered.date = this.checkDate(user.registered.date);
			html += window.tmpl('userItem', user);
		}

		this.tableBody.innerHTML = html;
	}

	checkDate(date) {
		try {
			let _date = new Date(date);
			let day = _date.getDate();
			let month = _date.getMonth();
			let year = _date.getFullYear();
			let array = [];
			month++;
			Number(day) < 10 ? array.push('0' + day) : array.push(day);
			Number(month) < 10 ? array.push('0' + month) : array.push(month);
			array.push(year);
			return array.join('/')
		} catch (e) {
			new Error(e.message);
			return date;
		}
	}

	maskPhone(str) {
		return str.replace(/[^\d]/g, '')
			.replace(/(\d)?(\d{2,3})(\d{2})(\d{2})(\d{3})/, '$1 ($2) $3-$4-$5')
			.replace(/^(\d\s)/, '+$1');
	}
}

export default UserList;

'use strict';
import {declOfNum} from '../utils/index.js';

const FEMALE = 'female';
const MALE = 'male';

class Statistic {
	constructor(props) {
		this.container = document.getElementById('statistic');
		this.props = props;
		this.statistic = {
			count: {
				total: this.props.info.results,
				female: 0,
				male: 0,
				forMore: '',
				nat: '',
				telegram: 0
			}
		};

		this.init();
	}

	init() {
		let nat = {};
		this.props.results.forEach((user) => {
			if (user.gender) {
				switch (user.gender.toLowerCase()) {
					case FEMALE:
						this.statistic.count.female++;
						break;
					case MALE:
						this.statistic.count.male++;
						break;
					default:
						break
				}
			}

			if (user.nat) {
				if (Object.keys(nat).indexOf(user.nat) + 1) {
					nat[user.nat]++;
				} else {
					nat[user.nat] = 1;
				}
			}
		});

		this.statistic.count.nat = this.toString(nat);

		switch (Math.max(this.statistic.count.female, this.statistic.count.male)) {
			case this.statistic.count.female:
				this.statistic.count.forMore = FEMALE;
				break;
			case this.statistic.count.male:
				this.statistic.count.forMore = MALE;
				break;
			default:
				break
		}

		this.render();
	}

	toString(obj) {
		let str = '<div>';
		for (let key in obj) {
			str += `<div>${key} - ${obj[key]} ${declOfNum(obj[key], ['пользователь', 'пользователя', 'пользователей'])}</div>`;
		}
		str += '</div>';
		return str;
	}

	render() {
		this.container.innerHTML = window.tmpl('statisticTable', this.statistic);
	}
}

export default Statistic;
